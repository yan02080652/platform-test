package com.tem.platform.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.iplatform.common.Page;
import com.iplatform.common.QueryDto;
import com.iplatform.common.cache.utils.RedisCacheUtils;
import com.iplatform.common.utils.LogUtils;
import com.iplatform.common.web.Message;
import com.tem.notify.api.MailService;
import com.tem.notify.api.SmsService;
import com.tem.notify.dto.TemplateMailDto;
import com.tem.platform.Constants;
import com.tem.platform.api.OrgService;
import com.tem.platform.api.OrgUserService;
import com.tem.platform.api.PartnerBpService;
import com.tem.platform.api.PartnerService;
import com.tem.platform.api.UserService;
import com.tem.platform.api.condition.UserCondition;
import com.tem.platform.api.dto.OrgDto;
import com.tem.platform.api.dto.OrgUserDto;
import com.tem.platform.api.dto.PartnerDto;
import com.tem.platform.api.dto.UserDto;
import com.tem.platform.security.authorize.PermissionUtil;
import com.tem.platform.util.GenSeqUtil;

/**
 * @author chenjieming
 */
@Controller
@RequestMapping("platform/user")
public class UserController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(UserController.class);
	
    @Autowired
    private UserService userService;

    @Autowired
    private OrgService orgService;

    @Autowired
    private OrgUserService orgUserService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private MailService mailService;

    @Autowired
    private PartnerBpService partnerBpService;
    
    @Autowired
    private PartnerService partnerService;

    @RequestMapping()
    public String index(Model model) {

        model.addAttribute("partnerId", super.getPartnerIdForOrg());
        return "platform/user/index.base";
    }

    @RequestMapping(value = "list")
    public String list(Model model, UserDto user, Integer pageIndex, Integer flag) {
        pageIndex = pageIndex == null ? 1 : pageIndex;
        // 当前组织Id
        Long orgId = user.getOrgId();
        OrgDto org = this.orgService.getById(orgId);

        Long partnerId = null;
        if(org != null){
            partnerId = org.getPartnerId();
        }else{
            partnerId = super.getPartnerIdForOrg();
        }

        if(partnerId == null){
            partnerId = getCurrentPartnerId();
        }

        // 当前组织的所有子组织Id
        List<Long> orgIds = new ArrayList<>();
        orgIds.add(orgId);
        //选择了部门
        if (org != null) {
            Map<String, Object> map = new HashMap<>(16);
            map.put("partnerId", partnerId);
            map.put("path", org.getPath());
            List<OrgDto> orgDtoList = orgService.getList(map);
            for (OrgDto orgDto : orgDtoList) {
                orgIds.add(orgDto.getId());
            }
        }
        if (orgIds.size() == 0) {
            orgIds = null;
        }
        //包含下级员工选项
        if (flag != null && flag == 1) {
            orgId = null;
        } else {
            orgIds = null;
        }

        QueryDto<UserCondition> queryDto = new QueryDto<>(pageIndex, Page.DEFAULT_PAGE_SIZE);

        UserCondition userCondition = new UserCondition();
        userCondition.setPartnerId(partnerId);
        userCondition.setFullname(user.getFullname());
        userCondition.setMobile(user.getMobile());
        userCondition.setOrgId(orgId);
        userCondition.setOrgIds(orgIds);
        List<Integer> statuss = new ArrayList<Integer>();
        statuss.add(1);
        statuss.add(0);
        statuss.add(-10);
        userCondition.setStatuss(statuss);
        queryDto.setCondition(userCondition);

        Page<UserDto> pageList = userService.queryListWithPage(queryDto);
        model.addAttribute("pageList", pageList);

        return "platform/user/user-list.jsp";
    }

    @RequestMapping(value = "/load")
    public String list(Model model, Long id) {

        UserDto vo = null;
        if (id != null) {
            vo = userService.getUser(id);

            List<OrgUserDto> orgUsers = orgUserService.getByPartnerIdAndUserId(vo.getPartnerId(), id);
            model.addAttribute("orgUsers", orgUsers);
          
            model.addAttribute("isOnly", orgUsers.size());
        }
        model.addAttribute("user", vo);
        return "platform/user/user-model.jsp";
    }

    @ResponseBody
    @RequestMapping(value = "save")
    public Message addOrUpdate(UserDto user) {

        if(user.getId() != null){
            user = this.updateUserObject(user);
        }

        user.setEditUser(super.getCurrentUser().getId());

        Long userId = userService.saveUser(user);

        if (user.getId() == null) {
            user.setId(userId);
            gotoInitPassword(user, true);
        }
        return Message.success(userId.toString());
    }


    /**
     * 更新用户
     * @param user
     * @return
     */
    private UserDto updateUserObject(UserDto user){

        UserDto userDto = userService.getUserBaseInfo(user.getId());

        userDto.setFullname(user.getFullname());
        userDto.setEmpCode(user.getEmpCode());
        userDto.setType(user.getType());
        userDto.setVipLevel(user.getVipLevel());
        userDto.setMobile(user.getMobile());
        userDto.setEmail(user.getEmail());
        userDto.setStatus(user.getStatus());
        userDto.setGender(user.getGender());

        return userDto;
    }

    @ResponseBody
    @RequestMapping("initPassword")
    public Message initPassword(Long id) {

        UserDto userdto = userService.getUser(id);
        boolean flag = gotoInitPassword(userdto, false);
        if (flag) {

            userdto.setActivation("0");
            userService.updateUser(userdto);

            return Message.success("初始化密码成功");
        }
        return Message.error("初始化密码失败");
    }
    
    @ResponseBody
    @RequestMapping("initAllUserPassword")
    public Message initAllUserPassword() {
    	LogUtils.warn(logger, "尝试重置企业所有用户的密码，执行人:{}, name: {}", 
    			super.getCurrentUser().getId(), super.getCurrentUser().getFullname());
    	
    	if(PermissionUtil.checkTrans("IPE_USER_M", "INIT_PWD") == 0) {
    		Long partnerId = super.getPartnerId();
    		String catchKey = "INIT_ALL_USER_PWD_" + partnerId;
    		RedisCacheUtils.ignoreEnv();
    		String lock = RedisCacheUtils.get(catchKey);
    		if(lock != null) {
    			return Message.warn("10分钟内请勿重复执行！");
    		}
    		
    		RedisCacheUtils.ignoreEnv();
    		RedisCacheUtils.put(catchKey, "1", 10 * 60);
    		
    		Long tmcId = this.partnerBpService.findTmcIdByPId(partnerId);
    		PartnerDto p = this.partnerService.findById(partnerId);
    		
    		if(p != null && super.getTmcId().equals(tmcId)) {
    			LogUtils.warn(logger, "执行重置企业所有用户的密码，执行人:{}, 企业: {}", 
            			super.getCurrentUser().getId() + " - " + super.getCurrentUser().getFullname(),
            			p.getId() + " - " + p.getName());
    			
    			Map<String, Object> param = new HashMap<>();
        		param.put("partnerId", partnerId);
        		param.put("status", "0");
        		
        		Map<Long, UserDto> userMap = new HashMap<>();
        		List<UserDto> users = userService.getList(param);
        		for(UserDto user : users) {
        			userMap.put(user.getId(), user);
        		}
        		
        		param.put("status", "1");
        		users = userService.getList(param);
        		for(UserDto user : users) {
        			userMap.put(user.getId(), user);
        		}
        		
        		StringBuilder sucMsg = new StringBuilder();
        		StringBuilder errMsg = new StringBuilder();
        		int i = 0;
        		for(UserDto user : userMap.values()) {
        			boolean flag = gotoInitPassword(user, true);
                    if (flag) {
                    	sucMsg.append(user.getFullname()).append(",");
                    } else {
                    	errMsg.append(user.getFullname()).append(",");
                    }
                    i++;
                    
                    // 短信流控限制，不能发太快
                    if(i % 10 == 0) {
                    	try {
                    		Thread.sleep(1000L);
                    	} catch(InterruptedException e) {}
                    } else {
                    	try {
                    		Thread.sleep(200L);
                    	} catch(InterruptedException e) {}
                    }
        		}
        		
        		String msg = "成功的用户：\n" + sucMsg.toString();
        		if(errMsg.length() > 0) {
        			msg += "\n失败的用户：\n" + errMsg.toString();
        		}
        		
        		return Message.success(msg);
    		} else {
        		LogUtils.warn(logger, "执行人的tmcId与被执行企业的tmcId不一致，执行人:{}, 企业: {}", 
            			super.getCurrentUser().getId(), p.getId());
        	}
    	}
    	
        return Message.error("非法操作");
    }

    private boolean gotoInitPassword(UserDto user, boolean isFirst) {

        String smsConstant = null;
        if(isFirst) {
            smsConstant = Constants.USER_NOTIFY;
        } else {
            smsConstant = Constants.USER_RESET_PASSWORD;
        }

        String code = GenSeqUtil.getGenSeq(6);
        Long userId = user.getId();
        if (!StringUtils.isEmpty(user.getMobile())) {

            String alias = partnerService.findById(user.getPartnerId()).getAlias();
            String[] arr = new String[]{alias, user.getMobile(), code,};
            if (smsService.sendSms(user.getMobile(), smsConstant, arr, user.getPartnerId())) {
                userService.updatePassword(userId, code);
                return true;
            }
        } else if (!StringUtils.isEmpty(user.getEmail())) {

            TemplateMailDto templateMailDto = new TemplateMailDto();
            templateMailDto.setTemplateCode("USER_EMAIL_PASSWORD");

            List<String> arrayList = new ArrayList<String>();
            arrayList.add(user.getEmail());
            templateMailDto.setToAddress(arrayList);

            Map<String, Object> args = new HashMap<>(16);
            args.put("code", code);
            templateMailDto.setArgs(args);
            if (mailService.sendMail(templateMailDto)) {
                userService.updatePassword(userId, code);
                return true;
            }
        }
        return false;
    }

    @RequestMapping("/addOrgUser")
    public String addOrgUser(Model model) {
        model.addAttribute("typeTitle", "新增");

        model.addAttribute("partnerType", super.getStringValue("type"));
        return "platform/user/orgUserDetail.jsp";
    }

    @RequestMapping("/editOrgUser/{id}")
    public String editOrgUser(Model model, @PathVariable("id") Long id) {

        model.addAttribute("typeTitle", "修改");
        OrgUserDto orgUserDto = orgUserService.getById(id);
        model.addAttribute("orgUserDto", orgUserDto);

        model.addAttribute("partnerType", super.getStringValue("type"));
        return "platform/user/orgUserDetail.jsp";
    }

    @ResponseBody
    @RequestMapping(value = "/saveOrUpdateOrgUser", method = RequestMethod.POST)
    public Message saveOrUpdateOrgUser(OrgUserDto orgUserDto) {

        Long partnerId = orgUserDto.getPartnerId();

        //如果是主部门 则要先将其他部门设置为非主部门
        if (orgUserDto.getMain() == 1) {
            orgUserService.updateAllNoMainByPartnerIdAndUserId(partnerId, orgUserDto.getUserId());

            //更新user主表
            UserDto user = userService.getUser(orgUserDto.getUserId());
            user.setOrgId(orgUserDto.getOrgId());
            user.setTitle(orgUserDto.getTitle());
            userService.updateUser(user);
        }
        if (orgUserDto.getId() == null) {
            Long id = orgUserService.insert(orgUserDto);

            return Message.success(id.toString());
        } else {
            orgUserService.update(orgUserDto);
            return Message.success(orgUserDto.getId().toString());
        }
    }

    @ResponseBody
    @RequestMapping(value = "/deleteOrgUser")
    public Message deleteOrgUser(@RequestParam("id") Long id) {

        if (orgUserService.deleteById(id)) {
            return Message.success("删除成功");
        }
        return Message.error("删除失败");
    }

    @ResponseBody
    @RequestMapping(value = "/setMainOrg")
    public Message setMainOrg(Long id) {

        OrgUserDto orgUser = orgUserService.getById(id);

        UserDto user = userService.getUser(orgUser.getUserId());
        user.setOrgId(orgUser.getOrgId());
        user.setTitle(orgUser.getTitle());
        userService.updateUser(user);

        orgUserService.updateAllNoMainByPartnerIdAndUserId(orgUser.getPartnerId(), orgUser.getUserId());
        orgUser.setMain(1);
        orgUserService.update(orgUser);
        return Message.success("设置成功");

    }

    /**
     * 验证手机号码是否可用
     *
     * @param mobile
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "checkMobile")
    public boolean checkMobile(String mobile, Long userId) {

        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        Map<String, Object> params = new HashMap<>(16);
        params.put("partnerId", partnerId);
        params.put("mobile", mobile);
        params.put("userId", userId);
        Integer count = userService.getUserCount(params);
        if (count == 0) {
            return true;
        }
        return false;
    }

    @ResponseBody
    @RequestMapping(value = "checkOrgUser")
    public boolean checkOrgUser(Long orgId, Long userId, Long orgUserId) {

        OrgUserDto orgUser = orgUserService.checkOrgUserByUserIdAndOrdId(userId, orgId, orgUserId);
        if (orgUser != null) {
            return false;
        }
        return true;
    }

    /**
     * empCode验证,用户编码
     *
     * @param empCode
     * @param userId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "checkEmpCode")
    public boolean checkEmpCode(String empCode, Long userId) {

        Long partnerId = super.getLongValue(Constants.PARAM_KEY_PARTNER);

        Map<String, Object> params = new HashMap<>(16);
        params.put("partnerId", partnerId);
        params.put("empCode", empCode);
        params.put("userId", userId);
        Integer count = userService.getUserCount(params);
        if (count == 0) {
            return true;
        }
        return false;
    }
}
